const cheerio = require('cheerio')
const request = require('request')

const fileExtensionRestrictions = /^.+(\.gif|\.ico|\.jpeg|\.jpg|\.pdf|\.png|\.txt|\.xml)$/ // Matches ['.gif', '.ico', '.jpeg', '.jpg', '.pdf', '.txt', '.xml']. Used in getInternalLinks().
const website = 'https://codev.com/' // Change value here for testing.
const splitUrl = website.split('://')
const protocol = splitUrl[0]
const domainName = splitUrl[1].replace(/www\.(.+)/, '$1') // Matches 'www.anydomainyoucanthinkof.com'.

// Check if sitemap exists and get contents if available.
const checkSiteMap = (website) => {
	const siteMapUrl = `${website}/sitemap.xml`

	return new Promise((resolve, reject) => {
		try {
			request(siteMapUrl, (err, res, body) => {
				if(!err && res.statusCode === 200) {
					resolve(body)
				}else {
					resolve(false)
				}
			})
		} catch(error) {
			reject(error)
		}
	})
}

// Crawl a web page and get html data.
const crawlWebsite = (website) => {
	return new Promise((resolve, reject) => {
		try {
			request(website, (err, res, body) => {
				if(!err && res.statusCode === 200) {
					resolve(body)
				}else {
					resolve(false)
				}
			})
		} catch(error) {
			reject(error)
		}
	})
}

// Get possible internal links based on the given html body.
const getInternalLinks = (htmlBody) => {
	// Load html in cheerio and use like jQuery.
	const $ = cheerio.load(htmlBody)
	const firstIndexesRegEx = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)$/ // Matches ['http://www', 'https://www', 'http://', 'https://'].
	const secondIndexesRegEx = /^(.+)(\/[\?\#].*|\/)$/ // Matches page anchors and URLs that passes parameters.
	let links = []
	let paths = []
	let path = ''
	let scannablePath = false

	// Loop through <a> tags and get the 'href' attribute.
	$('a').each((i, link) => {
		let href = $(link).attr('href')
		if(typeof href !== 'undefined' && href.match(domainName)) {
			links[i] = href
		}
	})

	// Just set the array to a cleaner version before proceeding to another process.
	const sanitizedLinks = sanitizeArray(links)

	// Loop through the links.
	// Remove third-party URLs and paths that redirect to a file.
	// Only get the scannable URLs
	for(let i=0; i < sanitizedLinks.length; i++) {
		let currentLink = sanitizedLinks[i].split(domainName)
		let firstIndex = currentLink[0]
		let secondIndex = currentLink[1]

		if(firstIndex === '' || firstIndex.match(firstIndexesRegEx)) {
			if(secondIndex && secondIndex !== '/') {
				path = secondIndex.replace(secondIndexesRegEx, '$1') // Remove path parameters and anchors.
				pathToTest = website + path
				scannablePath = !(pathToTest).match(fileExtensionRestrictions) ? pathToTest : false
				paths[i] = scannablePath
			}
		}
	}

	// Clean the array to remove empty values.
	return sanitizeArray(paths)
}

// Get listing of internal URLs via sitemap data.
const getInternalLinksXml = (xml) => {
	let links = []
	const $ = cheerio.load(xml,
		{
			xmlMode: true
		}
	)

	// Loop through <loc> tag.
	$('loc').each((i, link) => {
		links[i] = $(link).text()
	})

	return sanitizeArray(links)
}

// This is the main function.
const main = async () => {
	const siteMap = await checkSiteMap(website)
	let internalLinks = null
	
	// Check first if a website contains sitemap before doing manual crawl.
	// If sitemap exists, skip the crawling part.
	if(siteMap) {
		internalLinks = getInternalLinksXml(siteMap)
	}else {
		// Proceed to crawling through page html if sitemap is not available.
		const htmlBody = await crawlWebsite(website)
		internalLinks = getInternalLinks(htmlBody)
	}

	// Simulate recursive scanning.
	const simulateRecursion = (link) => {
		console.log(`Scanning [${link}]...`)

		if(internalLinks.length) {
				simulateRecursion(internalLinks.pop())
		}else {
			console.log('Scan sequence finished.')
		}
	}

	if(internalLinks.length) {
		console.log('Scan sequence started.')
		simulateRecursion(internalLinks.pop())
	}else {
		console.log('Nothing to scan.')
	}
}

// Process unclean array. Remove undefined, false data and duplicate values.
const sanitizeArray = (array) => {
	let sanitizedArray = array.filter(el => !(el === undefined || el === false))
	let result = Array.from(new Set(sanitizedArray))

	return result
}

// Run main function.
main();